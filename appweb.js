const express = require('express');
const qrcode = require('qrcode');
const { Client, NoAuth } = require('whatsapp-web.js');

const app = express();

// Crear una instancia del cliente de WhatsApp
const client = new Client({
    authStrategy: new NoAuth()
});

// Manejar el evento de generación de código QR
client.on('qr', async qr => {
    // Generar un código QR como una imagen en formato de datos URL
    const qrCodeImage = await qrcode.toDataURL(qr);
    io.emit('qr', qrCodeImage); // Emitir el código QR a través de Socket.io
});

// Manejar el evento de cliente listo
client.on('ready', () => {
    console.log('Client is ready!');
});

// Manejar el evento de recepción de mensaje
client.on('message', message => {
    console.log(message.body);

    if (message.body === '!ping') {
        client.sendMessage(message.from, 'hola bea como estas ');
    }
});

// Inicializar el cliente de WhatsApp
client.initialize();

// Configurar Express para servir archivos estáticos desde la carpeta 'public'
app.use(express.static('public'));

// Iniciar el servidor de Express
const server = app.listen(3000, () => {
    console.log('Servidor Express iniciado en el puerto 3000');
});

// Configurar Socket.io para manejar la conexión del cliente web
const io = require('socket.io')(server);

// Manejar la conexión de un cliente mediante Socket.io
io.on('connection', socket => {
    if (client.qrCode) {
        // Función asincrónica para generar el código QR y emitirlo a través de Socket.io
        const generateQRCode = async () => {
            const qrCodeImage = await qrcode.toDataURL(client.qrCode);
            socket.emit('qr', qrCodeImage);
        };

        // Llamar a la función para generar y emitir el código QR
        generateQRCode();
    }
});

// Endpoint para enviar mensajes mediante una solicitud POST
app.post('/send-message', express.json(), (req, res) => {
    const { recipient, text } = req.body;

    if (!recipient || !text) {
        return res.status(400).json({ error: 'Recipient and text are required' });
    }

    // Enviar el mensaje usando el cliente de WhatsApp
    client.sendMessage(recipient, text)
        .then(() => {
            res.json({ success: true, message: 'Message sent successfully' });
        })
        .catch(error => {
            res.status(500).json({ error: 'An error occurred while sending the message' });
        });
});
