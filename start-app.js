const path = require('path');
const Service = require('node-windows').Service;

// Crear un nuevo servicio
const svc = new Service({
    name: 'node-whatsapp', // Nombre del servicio
    description: 'Node.js application running my app', // Descripción del servicio
    script: path.join(__dirname, 'appwebmultidevice.js'), // Ruta al archivo principal de tu aplicación
});

// Instalar el servicio
svc.on('install', () => {
    svc.start();
    console.log('Service installed and started');
});

// Verificar si el servicio ya está instalado
if (svc.exists) {
    svc.start();
} else {
    svc.install();
}

/*
Una vez que hayas realizado estos pasos, tu aplicación Node.js se iniciará automáticamente al arrancar Windows. 
Puedes administrar el servicio utilizando comandos como 'node-whatsapp start', 'node-whatsapp stop',
'node-whatsapp restart', etc. Recuerda reemplazar node-whatsapp con el nombre que hayas especificado en tu script.
*/