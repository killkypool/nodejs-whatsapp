const express = require('express');
const qrcode = require('qrcode');
const cors = require('cors')
const { Client, NoAuth, LocalAuth  } = require('whatsapp-web.js');

const app = express();
app.use(cors())
const clients = {}; // Almacenar los clientes de WhatsApp en un objeto

// Crear una instancia del cliente de WhatsApp
const client = new Client({
    //authStrategy: new LocalAuth()
    authStrategy: new NoAuth()
});
var clientInstance = [];

// Manejar el evento de generación de código QR
client.on('qr', async qr => {
    // Generar un código QR como una imagen en formato de datos URL
    const qrCodeImage = await qrcode.toDataURL(qr);
  //  io.emit('qr', qrCodeImage); // Emitir el código QR a través de Socket.io
});

// Manejar el evento de cliente listo
client.on('ready', () => {
    console.log('Client is ready!');
});

// Manejar el evento de recepción de mensaje
client.on('message', message => {
    console.log(message.body);

    if (message.body === '!ping') {
        client.sendMessage(message.from, 'hola bea como estas ');
    }
});

// Inicializar el cliente de WhatsApp
client.initialize();

// Configurar Express para servir archivos estáticos desde la carpeta 'public'
app.use(express.static('public'));
/*
// Ruta para generar códigos QR para clientes específicos
app.get('/qr/:clientId', async (req, res) => {
    const clientId = req.params.clientId;
    console.log('######################-> ', clientId);

    let clientInstance = clients[clientId];

    if (!clientInstance) {
        // Si el cliente no existe, crear una nueva instancia y almacenarla en el objeto clients
        clientInstance = new Client({
            authStrategy: new NoAuth(),
            initialized: false // Agregar propiedad para rastrear el estado de inicialización
        });

        // Almacenar la promesa en la propiedad 'initializingPromise'
        clientInstance.initializingPromise = new Promise(async (resolve) => {
            clientInstance.on('qr', async qr => {
                console.log('async qr', qr);
                let qrCodeImage = await qrcode.toDataURL(qr);
                clientInstance.qrCodeImage = qrCodeImage;
                let qrCodeData = {
                    qrCode: clientInstance.qrCodeImage
                };
                resolve(qrCodeData); // Resolver la promesa una vez que el código QR esté listo
            });

            clientInstance.on('ready', () => {
                // Cliente listo para su uso, cambiar el estado de inicialización
                clientInstance.initialized = true;
            });

            // Inicializar el cliente
            await clientInstance.initialize();
        });

        // Almacenar la instancia de cliente en el objeto clients
        clients[clientId] = clientInstance;
    }

    // Esperar a que se resuelva la promesa de inicialización
    await clientInstance.initializingPromise;

    if (clientInstance.qrCodeImage) {
        let qrCodeData = {
            qrCode: clientInstance.qrCodeImage
        };
        res.json(qrCodeData);
    } else {
        res.status(404).json({ error: 'QR code not available for this client' });
    }
});
*/



app.get('/qr/:clientId', async (req, res) => {
    const clientId = req.params.clientId;
    console.log('######################-> ', clientId);

    // Verificar si ya existe un cliente para este ID
    let clientInstance = clients[clientId];

    if (!clientInstance) {
        clientInstance = new Client({
            authStrategy: new NoAuth()
        });

        // Manejar el evento de generación de código QR
        clientInstance.on('qr', async qr => {
            console.log(' async qr', qr);
            const qrCodeImage = await qrcode.toDataURL(qr);
            clientInstance.qrCodeImage = qrCodeImage;
            sendQRCodeResponse(res, clientInstance.qrCodeImage);
        });

        // Inicializar el cliente de WhatsApp
        clientInstance.initialize();

        // Guardar el cliente en el objeto de clientes
        clients[clientId] = clientInstance;
    } else {
        if (clientInstance.qrCodeImage) {
            sendQRCodeResponse(res, clientInstance.qrCodeImage);
        } else {
            res.status(404).json({ error: 'QR code not available for this client' });
        }
    }
});

function sendQRCodeResponse(res, qrCodeImage) {
    const qrCodeData = {
        qrCode: qrCodeImage
    };
    //res.json(qrCodeData);
}

// Nuevo endpoint para solicitar la creación de un nuevo cliente y su código QR
app.get('/create-client/:clientId', async (req, res) => {
    const clientId = req.params.clientId;

    if (!clients[clientId]) {
        const clientInstance = new Client({
            authStrategy: new NoAuth()
        });

        // Manejar el evento de generación de código QR
        clientInstance.on('qr', async qr => {
            console.log(' async qr', qr);
            const qrCodeImage = await qrcode.toDataURL(qr);
            clientInstance.qrCodeImage = qrCodeImage;

            // Emitir el código QR a todos los clientes conectados
            io.emit('qr', { clientId: clientId, qrCodeImage: qrCodeImage });

            sendQRCodeResponse(res, clientInstance.qrCodeImage);
        });

        // Inicializar el cliente de WhatsApp
        clientInstance.initialize();

        // Guardar el cliente en el objeto de clientes
        clients[clientId] = clientInstance;

        res.json({ success: true, message: 'Client created and QR code generated' });
    } else {
        res.json({ success: false, message: 'Client already exists' });
    }
});









// ...


app.get('/create-clientv2/:clientId', async (req, res) => {
    const clientId = req.params.clientId;
    console.log('########## clientId',clientId)

    if (!clients[clientId]) {
        const clientInstance = new Client({
            authStrategy: new NoAuth()
        });

        // Agregar propiedad de estado para rastrear si el QR ha sido escaneado
        clientInstance.qrScanned = false;
        clientInstance.isReady = false; // Inicialmente, el cliente no está listo

        // Manejar el evento de generación de código QR
        clientInstance.on('qr', async qr => {
            console.log('############## qr', qr);
                const qrCodeImage = await qrcode.toDataURL(qr);
                clientInstance.qrCodeImage = qrCodeImage;

                // Emitir el código QR solo si el cliente está listo
                    let objsocket={ clientId: clientId, qrCodeImage: qrCodeImage }
                    console.log('########## Mostrar QR',clientId)


                    io.emit('qr', objsocket);

                //sendQRCodeResponse(res, clientInstance.qrCodeImage);
        });

        // Manejar el evento cuando el código QR es escaneado
        clientInstance.on('authenticated', () => {
            console.log('########## Escaneando qr')
            clientInstance.qrScanned = true; // Marcar el QR como escaneado
        });

        // Inicializar el cliente de WhatsApp
        clientInstance.initialize();

        // Guardar el cliente en el objeto de clientes
        clients[clientId] = clientInstance;

        console.log('########## Client created and QR code generated')
        res.json({ success: true, message: 'Client created and QR code generated' });
    } else {
        console.log('########## Client already exists')
        res.json({ success: false, message: 'Client already exists' });
    }
});

// ...
















// Iniciar el servidor de Express
const server = app.listen(3000, () => {
    console.log('Servidor Express iniciado en el puerto 3000');
});

// Configurar Socket.io para manejar la conexión del cliente web
const io = require('socket.io')(server);
/*
// Manejar la conexión de un cliente mediante Socket.io
io.on('connection', socket => {
    // Agregar lógica aquí para manejar la conexión del cliente
    if (client.qrCode) {
        // Función asincrónica para generar el código QR y emitirlo a través de Socket.io
        const generateQRCode = async () => {
            const qrCodeImage = await qrcode.toDataURL(client.qrCode);
            socket.emit('qr', qrCodeImage);
        };

        // Llamar a la función para generar y emitir el código QR
        generateQRCode();
    }
});*/

// Endpoint para enviar mensajes mediante una solicitud POST
app.post('/send-message', express.json(), (req, res) => {
    const { recipient, to, text } = req.body;

    if (!recipient || !text) {
        return res.status(400).json({ error: 'Recipient and text are required' });
    }

    // Enviar el mensaje usando el cliente de WhatsApp
    // Asegúrate de que el cliente correspondiente esté disponible y listo para usar aquí
   //console.log(clients[recipient],req.body)
    if (clients[recipient]) {
        console.log(to,req.body)
        clients[recipient].sendMessage(to, text)
            .then(() => {
                res.json({ success: true, message: 'Message sent successfully' });
            })
            .catch(error => {
                res.status(500).json({ error: 'An error occurred while sending the message', message:error });
            });
    } else {
        res.status(404).json({ error: 'Client not found' });
    }
});
